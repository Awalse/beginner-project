
Start project

    - npm start to run app

Information 
    
    - Port 2000
    - use bussiness mail for mongo Atlas
    - node ver 16.10.0
    
    routes

        - /signUp -> for sign up an account to login and use chatroom
        - /login -> for login an account to chatroom
        - /chat -> the lobby chat for select the room to join
        - /chatroom -> the room for chat messages
        - / -> home nothing special
    db
        https://cloud.mongodb.com/v2/616cef3630702f0f99b26469#metrics/replicaSet/616cf1b55eabc53fbb848c14/explorer/Beginner
    
    p.s. credit

        frontend & Socket IO base on https://www.youtube.com/watch?v=jD7FnbI76Hg (Training Roadmap)
        Backend base on https://www.youtube.com/playlist?list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q (Training Roadmap)
    
    special credit 

        The Training Roadmap & advices (All from game development team) :> 
        https://stackoverflow.com/
        https://www.w3schools.com/



