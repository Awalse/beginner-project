const express = require('express');
const app = express();
const path = require('path')
const mongoose = require('mongoose');
const cookieParser = require("cookie-parser");

const session = require('./utils/session')

// Route
const userRoutes = require('./api/routes/user')
const roomRoutes = require('./api/routes/room')

// Set session 
app.use(session.session)

// Access cookie //uncomment when done with frontend
app.use(cookieParser())

// Set mongo db connection
mongoose.connect('mongodb+srv://test:' + process.env.MONGO_PWD + '@cluster0.nvysv.mongodb.net/' + process.env.MONGO_DB_NAME + '?retryWrites=true&w=majority')
mongoose.Promise = global.Promise;

// urlencoded
app.use(express.urlencoded({
    extended: false
}))
app.use(express.json())

// Route use
app.use('/login', session.checkSession, (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public/login.html'))
});

app.use('/chat', session.checkSession, (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public/chat.html'))
})

app.use('/chatroom', session.checkSession, (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public/chatroom.html'))
})

app.use('/signUp', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public/sign_up.html'))
})

app.use('/user', userRoutes);

app.use('/room', session.checkSession, roomRoutes);

app.use('/', (err, req, res, next) => {
    console.log(err.message);
    res.sendFile(path.join(__dirname, 'public/error.html'))
})

app.use('/', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public/index.html'))
})
// Export module 
module.exports = app;