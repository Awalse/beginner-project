const users = [];
// var offUser = [];

// Join user to chat
function userJoin(id, username, room) {
    const user = {
        id, username, room
    }

    users.push(user);

    // let x = offUser.filter((u) => {
    //     return u.username !== username;
    // })
    // offUser = x;

    return user;
}

// Get current user
function getCurrentUser(id) {
    return users.find(user => user.id === id);
}

// User leaves chat
function userLeave(id) {
    const index = users.findIndex(user => user.id === id);
    if (index !== -1) {
        let user = users.splice(index, 1)[0] //get the first one that u splits
        // offUser.push(user) // store offline user
        return user;
    }
}

// Get room users
function getRoomUser(room) {
    return users.filter(user => user.room === room).map(user => user.username)
}

// Get offline User
// function getOfflineUSer(room) {
//     return offUser.filter(user => user.room === room).map(user => user.username)
// }

module.exports = {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUser,
}