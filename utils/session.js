var session = require('express-session')
const MongoStore = require('connect-mongo')

const maxAge = 1000 * 60 * 60;

exports.session = session({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    cookie: {
        maxAge: maxAge
    },
    rolling: true,
    resave: false,
    store: MongoStore.create({
        mongoUrl: 'mongodb+srv://test:' + process.env.MONGO_PWD + '@cluster0.nvysv.mongodb.net/' + process.env.MONGO_DB_NAME + '?retryWrites=true&w=majority',
        ttl: maxAge,
        autoRemove: 'native'
    })
})

exports.checkSession = (req, res, next) => {
    if (req.session.userId) {
        return req.originalUrl == '/login' ? res.redirect('/') : next();
    } else {
        return req.originalUrl == '/login' ? next() : next(new Error('Session Expired'));
    }
}

exports.updateSession = (sockSession) => {
    let mysession = sockSession
    mysession.touch();
    mysession.save();
}



