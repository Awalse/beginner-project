const socketIo = require('socket.Io');
// users 
const { userJoin, getCurrentUser, getRoomUser, userLeave, getOfflineUSer } = require('../utils/user')
const formatMessages = require('../utils/message')
const session = require('../utils/session')

const botName = 'Bot';
const wrap = middleware => (socket, next) => middleware(socket.request, {}, next);
var timeoutId;
// Debug only
var intervalId;
function createSocket(server) {
    const io = socketIo(server)
    io.use(wrap(session.session));
    io.on('connection', (socket) => {
        const sockSession = socket.request.session;
        let x = sockSession.cookie.originalMaxAge
        startTimeout(x, socket)
        socket.use((packet, next) => {
            clearMyTimeout(timeoutId)
            startTimeout(x, socket)
            session.updateSession(sockSession);
            next()
        })
        // Join room
        socket.on('joinRoom', ({ username, room }) => {

            const user = userJoin(socket.id, username, room)

            socket.join(user.room)

            socket.emit('message', formatMessages(botName, 'Welcome to Chat!'))

            socket.broadcast.to(user.room)
                .emit('message', formatMessages(botName, user.username + ' has joined Room.'))

            io.to(user.room)
                .emit('roomUser', {
                    room: user.room,
                    users: getRoomUser(user.room),
                })
        })

        // Listen for chatMessage
        socket.on('chatMessage', (message) => {
            const user = getCurrentUser(socket.id);
            io.to(user.room)
                .emit('message', formatMessages(user.username, message));
        })

        // Runs when client disconnects
        socket.on('disconnect', () => {

            const user = userLeave(socket.id);

            if (user) {
                io.to(user.room)
                    .emit('message', formatMessages(botName, user.username + ' has left the chat'))


                // Send users and room info
                io.to(user.room)
                    .emit('roomUser', {
                        room: user.room,
                        users: getRoomUser(user.room),
                        // offUser: getOfflineUSer(user.room)
                    })
            }
        })
    })
}

module.exports = createSocket;

// Connection Timeout
function startTimeout(maxAge, socket) {
    timeoutId = setTimeout(() => {
        socket.emit('timeout', { message: 'connection error' })
    }, maxAge);
}

function clearMyTimeout(id) {
    clearTimeout(id)
}
