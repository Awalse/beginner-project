const http = require('http')
const app = require('./app')

const socket = require('./utils/socketIo')

const server = http.createServer(app);

socket(server);

const PORT = process.env.PORT || 5000;


server.listen(PORT, () => console.log('Server is running on PORT...' + PORT));