const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const User = require('../models/user');

// Save Session
var session;

// Export Function
exports.user_sign_up = (req, res, next) => {
    console.log(req);
    User.findOne({
        username: req.body.username,
    })
        .exec()
        .then(user => {
            // Condition when found user
            if (user) {
                return res.status(409).json({
                    message: "This username is exits."
                })
            } else {
                // Sign up
                bcryptjs.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        console.log(err)
                        return res.status(500).json({ message: err.message })
                    }

                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        username: req.body.username,
                        password: hash
                    })
                    user.save();
                    res.status(201).json({
                        message: 'Sign up complete. Redirect to Login.'
                    })
                })
            }
        })
}

exports.user_login = (req, res, next) => {
    User.findOne({
        username: req.body.username
    })
        .exec()
        .then(user => {
            if (!user) {
                return res.status(401).json({ message: "Login failed!" })
            } else {
                bcryptjs.compare(req.body.password, user.password, (err, result) => {
                    if (err) {
                        console.log(err)
                        return res.status(401).json({ message: "Login failed!" })
                    }

                    if (result) {
                        session = req.session;
                        session.userId = req.body.username
                        session.save();
                        res.status(200).json({ message: "Login Complete.", username: req.body.username });
                    } else {
                        return res.status(401).json({ message: "Login failed!" })
                    }
                })
            }
        })
}

exports.user_logout = (req, res, next) => {
    req.session.destroy(err => {
        if (err) next(err)
        res.clearCookie('connect.sid')
        res.status(200).json({ message: "User logged out!" })
    })
}

// Get rooms from specific user
exports.get_room = (req, res, next) => {
    if (!req.params.username) {
        return res.status(400).json({ message: 'Invalid username' })
    }

    User.findOne({ username: req.params.username })
        .exec()
        .then(result => {
            return res.status(200).json({ rooms: result.rooms })
        })
        .catch(err => {
            console.log(err)
            next(err)
        })
}

// Update session expired when use socket emit
exports.update = (req, res, next) => {
    res.status(200).json({ message: 'update' })
}