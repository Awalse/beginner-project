const mongoose = require('mongoose');
const room = require('../models/room');
const Room = require('../models/room');
const user = require('../models/user');
const User = require('../models/user');

exports.createRoom = (req, res, next) => {
    try {
        Room.findOne({
            roomName: req.body.roomName
        })
            .exec()
            .then(result => {
                let message = 'Room stored successfully'
                if (!result) {
                    // Save room before add to userStoreRoom
                    const room = new Room({
                        _id: new mongoose.Types.ObjectId(),
                        roomName: req.body.roomName,
                        users: [req.body.username]
                    })
                    message += ', Craeted new Room!'
                    room.save();
                } else {
                    storeUser(req.body.username, req.body.roomName)
                }
                userStoreRoom(req.body.username, req.body.roomName)
                return res.status(200).json({ message: message })
            })
    } catch (err) {
        next(err)
    }
}

exports.get_specific_room_info = (req, res, next) => {
    Room.findOne({
        roomName: req.params.roomname
    })
        .exec()
        .then(result => {
            let infos = result.users
            return res.status(200).json({ infos: infos })
        })
}

// Get all Exits room
exports.get_all_room = (req, res, next) => {
    Room.find({})
        .exec()
        .then(result => {
            let rooms = result.map(x => x.roomName)
            return res.status(200).json({ rooms: rooms })
        })
}

// Store rooms for specific user 
function userStoreRoom(username, room) {
    User.findOne({ username: username }).exec()
        .then(result => {
            if (!result.rooms.includes(room)) {
                User.updateOne({
                    username: username,
                }, {
                    $push: {
                        rooms: [room]
                    }
                }).exec()
            }
        }).catch(err => {
            console.log(err)
        })
}

function storeUser(username, room) {
    console.log(room)
    Room.findOne({ roomName: room })
        .exec()
        .then(result => {
            if (!result.users.includes(username)) {
                Room.updateOne({
                    roomName: room,
                }, {
                    $push: {
                        users: [username]
                    }
                }).exec()
            }
        }).catch(err => {
            console.log(err)
        })
}