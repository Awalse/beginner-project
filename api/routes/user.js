const express = require('express');
const router = express.Router();
const cors = require('cors');

const UserController = require('../controllers/user')

router.post('/signup', cors(), UserController.user_sign_up)

router.post('/login', cors(), UserController.user_login);

router.get('/logout', cors(), UserController.user_logout);

router.get('/room/:username', cors(), UserController.get_room)

router.get('/cookie', cors(), UserController.update)
// Export module 
module.exports = router