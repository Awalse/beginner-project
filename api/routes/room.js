const express = require('express');
const router = express.Router();
const cors = require('cors');

const RoomController = require('../controllers/room');

router.post('/created', cors(), RoomController.createRoom)

router.get('/specific/:roomname', cors(), RoomController.get_specific_room_info)

router.get('/', cors(), RoomController.get_all_room)

// Export module 
module.exports = router