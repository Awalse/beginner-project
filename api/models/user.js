const mongoose = require('mongoose');

// Create User Schema model
const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    rooms: [{
        type: String,
    }]
})

module.exports = mongoose.model('User', userSchema);

