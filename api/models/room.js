const mongoose = require('mongoose');

// Create User Schema model
const roomSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    roomName: {
        type: String,
        required: true,
        unique: true,
    },
    users: [{
        type: String,
    }]
})

module.exports = mongoose.model('Room', roomSchema);

